package net.techu;
import net.techu.data.ProductoMongo;
import net.techu.data.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
class ProductosV4Controller {
    @Autowired
    private ProductoRepository repository;
    /* Get lista de productos */
    @GetMapping(value = "/v4/productos", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> obtenerListado()
    {
        List<ProductoMongo> lista = repository.findAll();
        return new ResponseEntity<List<ProductoMongo>>(lista, HttpStatus.OK);
    }
    /* Add nuevo producto con nombre */
    @PostMapping(value="/v4/productos")
    public ResponseEntity<String> addProducto(@RequestBody ProductoMongo productoMongo)
    {
        ProductoMongo resultado = repository.insert(productoMongo);
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }
    @PutMapping(value="/v4/productos/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable String id, @RequestBody ProductoMongo productoMongo)
    {
        Optional<ProductoMongo> resultado = repository.findById(id);
        if (resultado.isPresent()) {
            ProductoMongo productoAModificar = resultado.get();
            productoAModificar.nombre = productoMongo.nombre;
//            productoAModificar.color = productoMongo.color;
            productoAModificar.precio = productoMongo.precio;
            ProductoMongo guardado = repository.save(resultado.get());
            return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
        }
        return new ResponseEntity<String>("Producto no encontrado", HttpStatus.NOT_FOUND);
    }
    @DeleteMapping(value="/v4/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable String id)
    {
        /* Posibilidad de buscar el producto, después eliminarlo si existe */
        repository.deleteById(id);
        return new ResponseEntity<String>("Producto borrado", HttpStatus.OK);
    }
    /* Get producto by nombre */
    @GetMapping(value = "/v4/productosbynombre/{nombre}", produces = "application/json")
    public ResponseEntity<ProductoMongo> obtenerProductoPorNombre(@PathVariable String nombre)
    {
        ProductoMongo resultado = repository.findByNombre(nombre);
        return new ResponseEntity<ProductoMongo>(resultado, HttpStatus.OK);
    }

}
