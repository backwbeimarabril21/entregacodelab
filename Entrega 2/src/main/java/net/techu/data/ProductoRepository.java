package net.techu.data;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRepository extends MongoRepository<ProductoMongo, String> {
    @Query("{'nombre':?0}")
    public ProductoMongo findByNombre(String nombre);

}
