package net.techu.data;

import org.springframework.data.mongodb.core.mapping.Document;

@Document("productosWbeimar")
public class ProductoMongo {

    public String id;
    public String nombre;
    public String precio;

    public ProductoMongo() {
    }

    public ProductoMongo(String nombre, String precio) {
        this.nombre = nombre;
        this.precio = precio;
    }

    @Override
    public String toString() {
        return String.format("Producto [id=%s, nombre=%s, precio=%s]", id, nombre, precio);
    }






}
