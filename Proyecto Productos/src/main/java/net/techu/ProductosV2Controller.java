package net.techu;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductosV2Controller {

    private ArrayList<Producto> listaProductos = null;

    public ProductosV2Controller() {
        listaProductos = new ArrayList<>();
        listaProductos.add(new Producto(1, "PR1",27.35));
        listaProductos.add(new Producto(2, "PR2", 18.33));
    }

//Get por ID
    @GetMapping("/v2/productos/{id}")
    public ResponseEntity<Producto> obtenerProductoPorId(@PathVariable int id)
    {
        Producto resultado = null;
        ResponseEntity<Producto> respuesta = null;
        try
        {
            resultado = listaProductos.get(id);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            respuesta = new ResponseEntity(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

    /* Add nuevo producto */
    @PostMapping(value = "/v2/productos", produces="application/json")
    public ResponseEntity<String> addProducto(@RequestBody Producto productoNuevo) {
        System.out.println("Estoy en añadir");
        System.out.println(productoNuevo.getId());
        System.out.println(productoNuevo.getNombre());
        System.out.println(productoNuevo.getPrecio());
        //listaProductos.add(new Producto(99, nombre, 100.5));
        listaProductos.add(productoNuevo);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    @PutMapping("/v2/productos/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable int id, @RequestBody Producto cambios)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            Producto productoAModificar = listaProductos.get(id);
            System.out.println("Voy a modificar el producto");
            System.out.println("Precio actual: " + String.valueOf(productoAModificar.getPrecio()));
            System.out.println("Precio nuevo: " + String.valueOf(cambios.getPrecio()));
            productoAModificar.setNombre(cambios.getNombre());
            productoAModificar.setPrecio(cambios.getPrecio());
            listaProductos.set(id, productoAModificar);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;

    }


    @DeleteMapping("/v2/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable int id)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            Producto productoAEliminar = listaProductos.get(id-1);
            listaProductos.remove(id-1);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;

    }
    @PatchMapping("/v2/productos/{id}")
    public ResponseEntity<String> patchProducto(@PathVariable int id, @RequestBody Producto productoOnline)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            Producto productoAModificar = listaProductos.get(id);
            System.out.println("Voy a patch el precio producto");
            System.out.println("Precio actual: " + String.valueOf(productoAModificar.getPrecio()));
            System.out.println("Precio nuevo: " + String.valueOf(productoOnline.getPrecio()));
            productoAModificar.setNombre(productoOnline.getNombre());
            productoAModificar.setPrecio(productoOnline.getPrecio());
            listaProductos.set(id, productoAModificar);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }


}
